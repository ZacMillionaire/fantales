
#-----Statement of Authorship----------------------------------------#
#
#  By submitting this task the signatories below agree that it
#  represents our own work and that we both contributed to it.  We
#  are aware of the University rule that a student must not
#  act in a manner which constitutes academic dishonesty as stated
#  and explained in QUT's Manual of Policies and Procedures,
#  Section C/5.3 "Academic Integrity" and Section E/2.1 "Student
#  Code of Conduct".
#
#  First student's no: n8824088
#  First student's name: Scott J. Schultz
#  Portfolio contribution: 100%
#
#  Second student's no: SOLO SUBMISSION
#
#  Contribution percentages refer to the whole portfolio, not just this
#  task.  Percentage contributions should sum to 100%.  A 50/50 split is
#  NOT necessarily expected.  The percentages will not affect your marks
#  except in EXTREME cases.
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  FANTALES
#
#  Movie fans have strong opinions about their favourite actors.  In
#  this task you and your programming partner will develop a program
#  that helps visualise some of the opinions of movie fans derived
#  from a survey of Microsoft employees.  To do so you will make use
#  of three different computer languages, Python, SQL and HTML.  You
#  will develop a Python function, show popularity, which accesses
#  data in an SQL database and uses this to generate HTML documents
#  which visually display an actor's popularity according to the
#  survey results.  See the instructions accompanying this file for
#  full details.
#
#--------------------------------------------------------------------#



#-----Acceptance Tests-----------------------------------------------#
#
#  This section contains unit tests that run your program.  You
#  may not change anything in this section.  NB: 'Passing' these
#  tests does NOT mean you have completed the assignment because
#  they do not check the HTML files produced by your program.
#
"""
------------------- Normal Cases with valid input --------------------

>>> show_popularity(['Female', 'Male', '30-40'], 20, 'Test01') # Test 1

>>> show_popularity(['20-30', '30-40', '40-50'], 50, 'Test02') # Test 2

>>> show_popularity(['20-40', '40-80', 'All'], 30, 'Test03') # Test 3

>>> show_popularity(['Female', 'Male', '30-40', '40-60', '60-100', 'All'], 30, 'Test04') # Test 4

>>> show_popularity(['All'], 20, 'Test05') # Test 5

>>> show_popularity(['30-40'], 50, 'Test06') # Test 6

>>> show_popularity(['30-50'], 0, 'Test07') # Test 7

------------------- Cases with invalid input ------------------------

>>> show_popularity(['20-30', '30-40', '3a-34' ], 30, 'Test08') # Test 8
Invalid customer group: 3a-34

>>> show_popularity(['teens', '20-20','30-40','40-50', '50-50', '60-d0'], 30, 'Test09') # Test 9
Invalid customer group: teens
Invalid customer group: 60-d0

>>> show_popularity(['old people', '30', '40-60', '-70', '70-100'], 30, 'Test10') # Test 10
Invalid customer group: old people
Invalid customer group: 30
Invalid customer group: -70

>>> show_popularity(['-', '30-50', '40-60', '50-20', '40 60'], 50, 'Test11') # Test 11
Invalid customer group: -
Invalid customer group: 40 60

""" 
#
#--------------------------------------------------------------------#



#-----Students' Solution---------------------------------------------#
#
#  Complete the task by filling in the template below.

#------------------------------------------------------------------------------#
#                                                                              #
#                                   IMPORTS                                    #
#                                                                              #
#------------------------------------------------------------------------------#

import mysql.connector, re, math, os

#------------------------------------------------------------------------------#
#                                                                              #
#                                   GLOBALS                                    #
#                                                                              #
#------------------------------------------------------------------------------#

database_object = {}

#------------------------------------------------------------------------------#
#                                                                              #
#                                  FUNCTIONS                                   #
#                                                                              #
#------------------------------------------------------------------------------#

def open_connection():
	"""
	Attempts to open a connection to the target database given login credentials.
	Fails and retuns any errors raised.
	On success this function will set the contents of database_object to contain
	the instance of the connection, and the cursor object.
	"""

	global database_object
	try:
		connection = mysql.connector.connect(
			host = "localhost",
			user = "root",
			password = "",
			db = "movie_survey"
		)
		cursor = connection.cursor()
		cursor.execute("SELECT VERSION()")
		result = cursor.fetchone()
	except Exception as e:
		return False
	except Error as database_error:
		print database_error
	else:
		database_object ={'connection': connection, 'cursor': cursor}
		return True

def close_connection():
	"""
	Closes the cursor and connection.
	Then empties the database_object dictionary for no real reason.
	"""

	global database_object
	# Close the cursor.
	database_object['cursor'].close()

	# Close the database connection.
	database_object['connection'].close()

	database_object.clear()

def query_database(query):
	"""
	Queries the database given an sql query.

	Problem is, there's no error checking here. Instead I sort of just brute-force
	validate pre-query data in the show_popularity() function which sort of works
	as query validation.
	"""

	# Execute an SQL query.
	database_object['cursor'].execute(query)

	# Retrieve the first row from the Result Set of the query.  Each row
	# is a list of the column values from the database.
	rows = database_object['cursor'].fetchall()

	# Display the first element of the row.
	return rows

def create_render_variables(actor_list):
	"""
	Generates the colour and size values for the actor list using em and hsl values.
	"""

	actor_dictionary = dict()

	# Old code for when I started at blue and decended down, now I just start at
	# 0 and go up. The maximum value is irrelavant, the hue value in hsl
	# wraps around after 360.

	hue_step = len(actor_list)
	hue_value = 0

	for i, row in enumerate(actor_list):

		em_scaling_factor = 0.15 # 0.1% Trust me on this, I know what I'm doing, I'm a doctor
		base_em_size = 4

		# What this does is see if the total number of actors tabulated
		# multiplied by the scaling factor is less than 1
		# If it is, we set the font_size to a baseline of 1em.
		# If we didn't, small aggregates of values would produce incredibly small
		# actor names, hurting readibility. As well as just looking silly~
		if base_em_size-(i*em_scaling_factor) < 1:
			font_size = "1em"
		else:
			font_size = str(base_em_size-(i*em_scaling_factor))+"em"


		actor_dictionary[str(row[0])] = {
			'hue': hue_value,
			'font_size': font_size
		}

		hue_value = hue_value + hue_step
	return actor_dictionary

def render_page(filename, render_list):
	"""
	Given a list of all data to be inserted and rendered to a html document,
	creates a page in the subdirectory created at runtime
	"""

	# The actors list, by design of the query, is ordered by actor count,
	# for output and display, the actor list needs to be in alphabetical order,
	# but colour and size needs to be done based off of the actor count.
	# To do this I run the create_render_variables function, which calculates the
	# values needed, then returns a dictionary

	actors_list = render_list[0]

	display_dict = create_render_variables(actors_list)

	# Now that I have my style values, I loop through the sorted actors_list,
	# and refer to values stored in the display_dict, in which values are identified
	# by the actors name, allowing for easy reference
	# There's probably a better way to do this.
	result_list = ''
	if actors_list:
		for row in sorted(actors_list):
			actor_name = row[0]
			actor_count = row[1]
			actor_style_data = display_dict[row[0]]

			font_size = actor_style_data['font_size']
			hue_value = actor_style_data['hue']

			# new lined and double tabbed because everyone loves nicely formatted html.
			# Everyone.
			result_list += "<div \
	class=\"actor-entry\" \
	style=\"font-size: {text_size}; color: hsl({text_hue},100%,50%)\">\
	{actor_name} ({actor_count})\
	</div>\n\t\t".format(
				text_size = font_size,
				text_hue = hue_value,
				actor_name = actor_name,
				actor_count = actor_count
			)
	else:
		result_list = ":("

	# Reference list for what value is what in the render_list object.
	# This'd be easier if dictionaries weren't so difficult.
	# To clarify, render_list is a list containing lists, which is apparently
	# impossible to simply create with named keys in a dictionary.
	# The work around doesn't particularly interest me either.

	# render_list = [
	# 	actor_list,
	# 	page_title,
	# 	focus,
	# 	actors_per_page,
	# 	customer_total,
	# 	link_list
	# ]

	# Now lets do what I said was impossible above in the worst way I know how.
	template_tag_dict = {
		"{title}":render_list[1],
		"{customer_group}":render_list[2],
		"{customer_count}":render_list[4],
		"{results_list}":result_list,
		"{prev_link}":render_list[5][0],
		"{next_link}":render_list[5][1]
	}

	# Open up our base template instead of having functions dedicated to ugly
	# strings of html.
	base_template = open('base template/html_template_base.html', 'r').read()

	# Create an output file using the subdirectory we created earlier
	output_file = open('html/'+filename,'w')

	# Just incase this was going to get to be a big function, I renamed
	# the base_template to rendered_page so I didn't get confused after drinking.
	# That didn't help because now I'm more confused looking back at this.
	rendered_page = base_template
	for key, value in template_tag_dict.items():
		rendered_page = rendered_page.replace(str(key), str(value))
	output_file.write(rendered_page)

def build_link_list(focus_list, filename, list_key):
	"""
	Builds a list of page navigation links, as well as preventing creation of
	pointless links at the start and end of a series of pages.
	"""

	link_list = []
	if len(focus_list) > 1:
		if list_key == 0:
			link_list = [
				'',
				'<a href="./{filename}_{focus_next}.html">Next Page</a>'.format(
					filename = filename,
					focus_next = focus_list[list_key+1]
				)
			]
		if list_key > 0 and list_key < len(focus_list)-1:
			link_list = [
				'<a href="./{filename}_{focus_prev}.html">Previous Page</a>'.format(
					filename = filename,
					focus_prev = focus_list[list_key-1]
				),
				'<a href="./{filename}_{focus_next}.html">Next Page</a>'.format(
					filename = filename,
					focus_next = focus_list[list_key+1]
				)
			]
		if list_key == len(focus_list)-1:
			link_list = [
				'<a href="./{filename}_{focus_prev}.html">Previous Page</a>'.format(
					filename = filename,
					focus_prev = focus_list[list_key-1]
				),''
			]
	else:
		link_list = [
			'',''
		]
	return link_list

def prep_page(filename,focus,query,actors_per_page,link_list):
	"""
	Prepares the meta data of the page, in this case the filename and the page title;
	runs the query function given a query; and calculates the total number of customers
	queried to get our result.

	Then calls the render_page() function to create the html file on the drive.
	"""

	output_filename = "{filename}_{focus_group}.html".format(
		filename = filename,
		focus_group = focus
	)

	page_title = "Top {actors_per_page} Most Popular Actors".format(
		actors_per_page = actors_per_page
	)

	# run our query
	actor_list = query_database(query)

	# calculate the total number of customers who voted
	customer_total = 0
	for row in actor_list:
		customer_total += row[1]
	# I miss key/value arrays, dictionaries aren't the same, too awkward.
	# you'll see why when you go back to the render_page function
	render_list = [
		actor_list,
		page_title,
		focus,
		actors_per_page,
		customer_total,
		link_list
	]
	render_page(output_filename, render_list)

##### PUT YOUR show_popularity FUNCTION HERE
def show_popularity(focus_list, actors_per_page, filename):
	"""
	Accepts 3 parameters and creates a collection of linked html documents.
	focus_list:
		A list of database constraints to filter customer results. Each item
		will also be appended to the filename.
		Accepts:
			* [int(age_min)-int(age_max)]
			* keywords
				- Female
				- Male
				- All
	actors_per_page:
		An integer to limit rows returned
		Accepts:
			* int(rows_to_return)
	filename:
		The filename to prepend to each of the focus_list groups, followed by
		the appropriate focus_list item, terminated by .html
		Accepts:
			* Strings, ints
	"""

	# Quick bit of house cleaning, create a destination folder for rendered pages
	# unless you really love having html files along side this source file.
	if not os.path.exists('html/'):
		os.makedirs('html/')

	# Open a connection to the database and get our database objects
	# print an error on connection failure.
	if not open_connection():
		print """Unable to connect to the database.
		I assumed the login details to be:
		\thost: localhost
		\tusername: root
		\tpassword: '<empty>'
		\tdatabase: movie_survey
		If this is incorrect please change the values to the correct details in
		the open_connection() function, starting at line 120."""
		return

	# An empty list to hold dead values for error output
	dead_values = []
	for key, focus in enumerate(focus_list):
		range_test = re.search("([0-9]+)-([0-9]+)",focus)
		if range_test:
			pass
		elif focus == "Female" or focus == "Male":
			pass
		elif focus == "All":
			pass
		else:
			print 'Invalid customer group: {focus}'.format(
				focus = focus
			)
			dead_values.append(focus)

	# Loop through our dead_values list and remove any matches within the focus list
	for value in dead_values:
		focus_list.remove(value)

	# Now with our clean focus list, build all valid pages using ~magic~
	for key, focus in enumerate(focus_list):

		range_test = re.search("([0-9]+)-([0-9]+)",focus)

		if range_test:

			link_list = build_link_list(focus_list, filename, key)
			min_age = range_test.group(1)
			max_age = range_test.group(2)

			# Swap age values if one is higher than the other
			if min_age > max_age:
				min_age, max_age = max_age, min_age

			# 2 queries for getting actors and customer counts? Nah, I can do it
			# in one.
			query = """SELECT `actor`, COUNT(`actor`) as 'actor_count'
					   FROM `favorite_actors`
					   INNER JOIN `customers` USING(`customerID`)
					   WHERE `age` >= {min_age} AND `age` <= {max_age}
					   GROUP BY `actor`
					   ORDER BY `actor_count` DESC
					   LIMIT {limit};
					""".format(
							min_age = min_age,
							max_age = max_age,
							limit = actors_per_page
						)
			prep_page(filename,focus,query,actors_per_page,link_list)

		elif focus == "Female" or focus == "Male":

			link_list = build_link_list(focus_list, filename, key)
			query = """SELECT `actor`, COUNT(`actor`) as 'actor_count'
					   FROM `favorite_actors`
					   INNER JOIN `customers` USING(`customerID`)
					   WHERE `gender` >= '{gender}'
					   GROUP BY `actor`
					   ORDER BY `actor_count` DESC
					   LIMIT {limit};
					""".format(
							gender = focus,
							limit = actors_per_page
						)
			prep_page(filename,focus,query,actors_per_page,link_list)

		elif focus == "All":

			link_list = build_link_list(focus_list, filename, key)
			query = """SELECT `actor`, COUNT(`actor`) as 'actor_count'
					   FROM `favorite_actors`
					   INNER JOIN `customers` USING(`customerID`)
					   GROUP BY `actor`
					   ORDER BY `actor_count` DESC
					   LIMIT {limit};
					""".format(
							limit = actors_per_page
						)
			prep_page(filename,focus,query,actors_per_page,link_list)

	close_connection()

#
#--------------------------------------------------------------------#



#-----Automatic Testing----------------------------------------------#
#
#  The following code will automatically run the unit tests
#  when this program is "run".  Do not change anything in this
#  section.  If you want to prevent the tests from running, comment
#  out the code below, but ensure that the code is uncommented when
#  you submit your program.
#

if __name__ == "__main__":
     from doctest import testmod
     testmod(verbose=True)  



#
#--------------------------------------------------------------------#
